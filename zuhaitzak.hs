data Zuhbit t = Zhutsa | Eraiki(t, Zuhbit t, Zuhbit t)

erroa :: (Zuhbit t) -> t
erroa(Zhutsa) = error "Zuhaitz husta. Ez dago errorik."
erroa(Eraiki(x,a,b) = x

ezker :: (Zuhbit t) -> t
ezker(Zhutsa) = error "Zuhaitz hutsa. Ez dago ezkerreko azpizuahitzik."
ezker(Eraiki(x,a,b)) = a

eskum :: (Zuhbit t) -> t
eskum(Zhusta) = error "Zuhaitz hutsa. Ez dago eskumako azpizuahitzik."
eskum(Eraiki(x,a,b)) = b
