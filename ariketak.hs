--ZERRENDA GAINEKO ERAGIKETAK
leh([]) = error "Zerrenda hutsa."
leh(x:s) = x

hond([]) = error "Zerrenda hutsa."
hond(x:s) = s

hutsa_da([]) = True
hutsa_da(x:s) = False

badago(x, []) = False
badago(x, y:s)
    | x == y    = True
    | x /= y    = badago(x,s)

luzera([]) = 0
luzera(x:r) = 1 + luzera(r)

bikoitia(x)
    | x `mod` 2 == 0    = True
    | otherwise         = False

bakoitia(x) = not(bikoitia(x))

bikop([]) = 0
bikop(x:r)
    | bikoitia(x)   = 1 + bikop(r)
    | bakoitia(x)   = bikop(r)

--inkr
inkr([]) = []
inkr(x:s) = (x + 1):inkr(s)

--batu
batu([]) = 0
batu(x:s) = x + batu(s)

--bikoitirik
bikoitirik([]) = False
bikoitirik(x:s)
    | bikoitia(x)       = True
    | bikoitirik(s)     = True
    | otherwise         = False 

--bik_posi
bik_posi(x:s, y:t)
    | bikoitia(x) /= bikoitia(y)    = False
    | luzera(s) /= luzera(t)        = error "luzera ezberdina"
    | 
    
--azkena
--azkena_kendu
--alder
--alder_dira
--aldiz
--erre
--kendu
--bik_kendu
--pos_bik_kendu
--pos_bak_kendu
--erre_kendu
--erre_kendu2
--bikote_berdin
--aurrizkia
--azpier
--elem_pos
--sartu
--lehen_bik_pos
--azken_pos
--hand
--elkartu
